valgrind ./cmake-build-debug/poly SimpleAddTest
 echo "Ran test SimpleAddTest"
valgrind ./cmake-build-debug/poly SimpleAddMonosTest
 echo "Ran test SimpleAddMonosTest"
valgrind ./cmake-build-debug/poly SimpleMulTest
 echo "Ran test SimpleMulTest"
valgrind ./cmake-build-debug/poly SimpleNegTest
 echo "Ran test SimpleNegTest"
valgrind ./cmake-build-debug/poly SimpleSubTest
 echo "Ran test SimpleSubTest"
valgrind ./cmake-build-debug/poly SimpleNegGroup
 echo "Ran test SimpleNegGroup"
valgrind ./cmake-build-debug/poly SimpleDegByTest
 echo "Ran test SimpleDegByTest"
valgrind ./cmake-build-debug/poly SimpleDegTest
 echo "Ran test SimpleDegTest"
valgrind ./cmake-build-debug/poly SimpleDegGroup
 echo "Ran test SimpleDegGroup"
valgrind ./cmake-build-debug/poly SimpleIsEqTest
 echo "Ran test SimpleIsEqTest"
valgrind ./cmake-build-debug/poly SimpleAtTest
 echo "Ran test SimpleAtTest"
valgrind ./cmake-build-debug/poly OverflowTest
 echo "Ran test OverflowTest"
valgrind ./cmake-build-debug/poly SimpleArithmeticTest
 echo "Ran test SimpleArithmeticTest"
valgrind ./cmake-build-debug/poly LongPolynomialTest
 echo "Ran test LongPolynomialTest"
valgrind ./cmake-build-debug/poly AtTest1
 echo "Ran test AtTest1"
valgrind ./cmake-build-debug/poly AtTest2
 echo "Ran test AtTest2"
valgrind ./cmake-build-debug/poly AtGroup
 echo "Ran test AtGroup"
valgrind ./cmake-build-debug/poly DegreeOpChangeTest
 echo "Ran test DegreeOpChangeTest"
valgrind ./cmake-build-debug/poly DegTest
 echo "Ran test DegTest"
valgrind ./cmake-build-debug/poly DegByTest
 echo "Ran test DegByTest"
valgrind ./cmake-build-debug/poly DegGroup
 echo "Ran test DegGroup"
valgrind ./cmake-build-debug/poly MulTest1
 echo "Ran test MulTest1"
valgrind ./cmake-build-debug/poly MulTest2
 echo "Ran test MulTest2"
valgrind ./cmake-build-debug/poly AddTest1
 echo "Ran test AddTest1"
valgrind ./cmake-build-debug/poly AddTest2
 echo "Ran test AddTest2"
valgrind ./cmake-build-debug/poly SubTest1
 echo "Ran test SubTest1"
valgrind ./cmake-build-debug/poly SubTest2
 echo "Ran test SubTest2"
#valgrind ./cmake-build-debug/poly ArithmeticGroup
# echo "Ran test ArithmeticGroup"
valgrind ./cmake-build-debug/poly IsEqTest
 echo "Ran test IsEqTest"
valgrind ./cmake-build-debug/poly RarePolynomialTest
 echo "Ran test RarePolynomialTest"
valgrind ./cmake-build-debug/poly MemoryThiefTest
 echo "Ran test MemoryThiefTest"
valgrind ./cmake-build-debug/poly MemoryFreeTest
 echo "Ran test MemoryFreeTest"
valgrind ./cmake-build-debug/poly MemoryGroup
 echo "Ran test MemoryGroup"