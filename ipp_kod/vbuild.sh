rm -r debug
mkdir debug
cd debug
cmake -D CMAKE_BUILD_TYPE=Debug ..
make
valgrind -s --leak-check=full ./poly
