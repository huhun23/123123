#include <stdio.h>
#include <stdlib.h>

struct S;

typedef struct S{
  int x, y;
  struct S* p;
} S;

S *test(){
  S* stp = malloc(sizeof(S));
  stp->x = 314;
  stp->y = 23;
  stp->p = malloc(4 * sizeof(S));
  return stp;
}

void delete(const S* p){
  free(p->p);
}

int main(){
  S* s = test();
  delete(s);
  free(s);
}