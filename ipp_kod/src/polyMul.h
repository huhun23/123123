#ifndef POLYNOMIALS_POLYMUL_H
#define POLYNOMIALS_POLYMUL_H


Poly PolyMulHelp(Poly *p1, Poly *q1);

/**
 * Mnoży dwa wielomiany.
 * @param[in] p : wielomian @f$p@f$
 * @param[in] q : wielomian @f$q@f$
 * @return @f$p * q@f$
 */
static inline Poly PolyMul(const Poly *p, const Poly *q){
    Poly p1 = PolyClone(p), q1 = PolyClone(q);

    Poly new = PolyMulHelp(&p1, &q1);
    PolySort(&new);

    return new;
}

static inline Poly PolyMulInactive(const Poly *p, const Poly *q){
  puts("Wielomian 1:");
  PolyPrint(*p, 0);
  puts("Wielomian 2:");
  PolyPrint(*q, 0);
  Poly p1 = PolyClone(p), q1 = PolyClone(q);
  Poly new = PolyMulHelp(&p1, &q1);
  PolySort(&new);
  puts("Wynik:");
  PolyPrint(new, 0);
  return new;
}

#endif //POLYNOMIALS_POLYMUL_H