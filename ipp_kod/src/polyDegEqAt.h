#ifndef POLYNOMIALS_POLYDEGEQAT_H
#define POLYNOMIALS_POLYDEGEQAT_H

poly_exp_t PolyLayerSimpleDeg(Poly *p);

poly_exp_t PolyDegBySorted(Poly *p, size_t var_idx);

/**
 * Zwraca stopień wielomianu ze względu na zadaną zmienną (-1 dla wielomianu
 * tożsamościowo równego zeru). Zmienne indeksowane są od 0.
 * Zmienna o indeksie 0 oznacza zmienną główną tego wielomianu.
 * Większe indeksy oznaczają zmienne wielomianów znajdujących się
 * we współczynnikach.
 * @param[in] p : wielomian
 * @param[in] var_idx : indeks zmiennej
 * @return stopień wielomianu @p p z względu na zmienną o indeksie @p var_idx
 */
static inline poly_exp_t PolyDegBy(const Poly *p, size_t var_idx){
    if(PolyIsZero(p)) return -1;
    else if(PolyIsCoeff(p)) return 0;

    Poly p1 = PolyClone(p);
    PolySort(&p1);
    poly_exp_t res = PolyDegBySorted(&p1, var_idx);
    PolyDestroy(&p1);
    return res;
}

poly_exp_t PolyDegSorted(Poly *p);

/**
 * Zwraca stopień wielomianu (-1 dla wielomianu tożsamościowo równego zeru).
 * @param[in] p : wielomian
 * @return stopień wielomianu @p p
 */
static inline poly_exp_t PolyDeg(const Poly *p){
    if(PolyIsZero(p)) return -1;
    else if(PolyIsCoeff(p)) return 0;

    Poly p1 = PolyClone(p);
    PolySort(&p1);
    poly_exp_t res = PolyDegSorted(&p1);
    PolyDestroy(&p1);

    return res;
}

bool PolyIsEqToCoeff(Poly *p, poly_coeff_t x);

bool PolyIsEqSorted(Poly *p1, Poly *q1);

/**
 * Sprawdza równość dwóch wielomianów.
 * @param[in] p : wielomian @f$p@f$
 * @param[in] q : wielomian @f$q@f$
 * @return @f$p = q@f$
 */
static inline bool PolyIsEq(const Poly *p, const Poly *q){
    Poly p1 = PolyClone(p), q1 = PolyClone(q);
    PolySort(&p1);
    PolySort(&q1);

    bool res = PolyIsEqSorted(&p1, &q1);

    PolyDestroy(&p1);
    PolyDestroy(&q1);
    return res;
}

static inline poly_coeff_t power(poly_coeff_t base, poly_exp_t n){
    if(n == 0){
        return 1;
    }
    else{
        poly_coeff_t b = base;
        for(poly_exp_t i = 1; i < n; ++i, base *= b);
        return base;
    }
}

/**
 * Wylicza wartość wielomianu w punkcie @p x.
 * Wstawia pod pierwszą zmienną wielomianu wartość @p x.
 * W wyniku może powstać wielomian, jeśli współczynniki są wielomianami.
 * Wtedy zmniejszane są o jeden indeksy zmiennych w takim wielomianie.
 * Formalnie dla wielomianu @f$p(x_0, x_1, x_2, \ldots)@f$ wynikiem jest
 * wielomian @f$p(x, x_0, x_1, \ldots)@f$.
 * @param[in] p : wielomian @f$p@f$
 * @param[in] x : wartość argumentu @f$x@f$
 * @return @f$p(x, x_0, x_1, \ldots)@f$
 */
Poly PolyAt(const Poly *p, poly_coeff_t x);

#endif //POLYNOMIALS_POLYDEGEQAT_H