#ifndef POLYNOMIALS_POLYNEGSUB_H
#define POLYNOMIALS_POLYNEGSUB_H

#include "polyAdd.h"

void PolyNegPtr(Poly *p);

static inline void MonoNeg(Mono *m){
    PolyNegPtr(&(m->p));
}

/**
 * Zwraca przeciwny wielomian.
 * @param[in] p : wielomian @f$p@f$
 * @return @f$-p@f$
 */
Poly PolyNeg(const Poly *p);
/**
 * Odejmuje wielomian od wielomianu.
 * @param[in] p : wielomian @f$p@f$
 * @param[in] q : wielomian @f$q@f$
 * @return @f$p - q@f$
 */
static inline Poly PolySub(const Poly *p, const Poly *q){
    Poly q1 = PolyClone(q);
    PolyNegPtr(&q1);
    Poly new = PolyAdd(p, &q1);
    PolyDestroy(&q1);
    return new;
}

#endif //POLYNOMIALS_POLYNEGSUB_H

