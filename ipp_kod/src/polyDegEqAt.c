#include "poly.h"
#include "polyDegEqAt.h"
#include "polyAdd.h"
#include "polyMul.h"

poly_exp_t PolyLayerSimpleDeg(Poly *p){ //output to -1 v 0 v 1
    if(p->arr == NULL && p->coeff == 0){
        return -1;
    }
    else if(p->arr == NULL){
        return 0;
    }
    else{
        poly_exp_t res = 0;
        for(poly_exp_t i = 0; res < 1 && i < p->size; ++i){
            if(p->arr[i].p.coeff = 0)
                res = -1;
            else if(p->arr[i].exp = 0)
                res = (poly_exp_t) max((size_t) res, 0);
            else
                res = 1;
        }
        return res;
    }
}

poly_exp_t PolyDegBySorted(Poly *p, size_t var_idx){
    if(p->arr == NULL && p->coeff == 0){
        return -1;
    }
    else if(p->arr == NULL){
        return 0;
    }
    else{
        if(var_idx == 0){
            return p->arr[p->size - 1].exp;
        }
        else{
            poly_exp_t max = 0, foo = 0;
            for(poly_exp_t i = 0; i < p->size; ++i){
                if((foo = PolyDegBySorted(&(p->arr[i].p), var_idx - 1)) > max)
                    max = foo;
            }
            return max;
        }
    }
}

poly_exp_t PolyDegSorted(Poly *p){
    if(p->arr == NULL && p->coeff == 0){
        return -1;
    }
    else if(p->arr == NULL){
        return 0;
    }
    else{
        poly_exp_t max = -1, foo = 0, res = 0;
        for(poly_exp_t i = 0; i < p->size; ++i){
            if((foo = PolyDegSorted(&(p->arr[i].p))) == -1)
                res = -1;
            else
                res = foo + p->arr[i].exp;

            if(res > max)
                max = res;
        }

        return max;
    }
}

bool PolyIsEqToCoeff(Poly *p, poly_coeff_t x){
    //wielomian p posortowany
    if(PolyIsCoeff(p)){
        return p->coeff == x;
    }

    poly_coeff_t deg = PolyDeg(p);

    if(deg == -1 && x == 0){
        return true;
    }
    else if(deg == -1 || x == 0){
        return false;
    }
    else if(deg == 0){
        return PolyIsEqToCoeff(&(p->arr[0].p), x);
    }
    else{
        return false;
    }
}

bool PolyIsEqSorted(Poly *p1, Poly *q1){
    bool equal = true;

    if(!PolyIsCoeff(p1) && !PolyIsCoeff(q1)){
        size_t i = 0, j = 0;

        while(equal && i < p1->size && j < q1->size){
            if(p1->arr[i].exp == q1->arr[j].exp){
                equal = equal && PolyIsEqSorted(&(p1->arr[i].p),&(q1->arr[j].p));
                i++;
                j++;
            }
            else if(p1->arr[i].exp < q1->arr[j].exp){
                equal = equal && PolyDegSorted(&(p1->arr[i].p)) == -1;
                i++;
            }
            else{
                equal = equal && PolyDegSorted(&(q1->arr[j].p)) == -1;
                j++;
            }
        }

        if(i >= p1->size){
            for(;j < q1->size; j++) equal &= PolyDegSorted(&(q1->arr[j].p)) == -1;
        }
        else{
            for(;i < p1->size; i++) equal &= PolyDegSorted(&(p1->arr[i].p)) == -1;
        }

        return equal;
    }
    else if(!PolyIsCoeff(q1)){
        return PolyIsEqSorted(q1, p1);
    }
    else if(!PolyIsCoeff(p1)){ //p1 nie jest współczynnikiem
        return PolyIsEqToCoeff(p1, q1->coeff);
    }
    else{
        return p1->coeff == q1->coeff;
    }
    return false;
}

Poly PolyAt(const Poly *p, poly_coeff_t x){
  Poly new, constant = PolyZero();

  if(PolyIsCoeff(p)){
    new = *p;
  }
  else{
    Poly p1 = PolyClone(p);
    PolySort(&p1);

    int old_size = 0;
    bool first = true;

    PolyArrAlloc(&new, p1.arr[0].p.size);

    for(size_t i = 0; i < p1.size; ++i){
      Poly p_coeff = PolyFromCoeff(power(x, p1.arr[i].exp));
      p1.arr[i].p = PolyMulHelp(&(p1.arr[i].p), &p_coeff); //mnożymy x^n * wielomian przy jednomianie

      if(PolyIsCoeff(&(p1.arr[i].p))){
        constant = PolyAddSorted(&constant, &(p1.arr[i].p));
      }
      else if(first) {
        for(int j = 0; j < new.size; j++) new.arr[j] = p1.arr[i].p.arr[j];
        first = false;
      }
      else {
        old_size = new.size;
        PolyReall(&new, old_size + p1.arr[i].p.size);
        for(int j = old_size; j < new.size; ++j) new.arr[j] = p1.arr[i].p.arr[j - old_size];
      }
        PolyLayerSort(&new);
    }
    for(int i = 0; i < p1.size; i++) free(p1.arr[i].p.arr);
    free(p1.arr);
  }
  new = PolyAddSorted(&new, &constant);

  return new;
}

