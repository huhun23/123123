/** @file
  Interfejs klasy wielomianów rzadkich wielu zmiennych

  @authors Jakub Pawlewicz <pan@mimuw.edu.pl>, Marcin Peczarski <marpe@mimuw.edu.pl>
  @copyright Uniwersytet Warszawski
  @date 2021
*/

#ifndef __POLY_H__
#define __POLY_H__

#define CHECK_PTR(p)  \
  do {                \
    if (p == NULL) {  \
      exit(1);        \
    }                 \
  } while (0)

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>


/** To jest typ reprezentujący współczynniki. */
typedef long poly_coeff_t;

/** To jest typ reprezentujący wykładniki. */
typedef int poly_exp_t;

struct Mono;

/**
 * To jest struktura przechowująca wielomian.
 * Wielomian jest albo liczbą całkowitą, czyli wielomianem stałym
 * (wtedy `arr == NULL`), albo niepustą listą jednomianów (wtedy `arr != NULL`).
 */
typedef struct Poly {
  /**
  * To jest unia przechowująca współczynnik wielomianu lub
  * liczbę jednomianów w wielomianie.
  * Jeżeli `arr == NULL`, wtedy jest to współczynnik będący liczbą całkowitą.
  * W przeciwnym przypadku jest to niepusta lista jednomianów.
  */
  union {
    poly_coeff_t coeff; ///< współczynnik
    size_t       size; ///< rozmiar wielomianu, liczba jednomianów
  };
  /** To jest tablica przechowująca listę jednomianów. */
  struct Mono *arr;
} Poly;

/**
 * To jest struktura przechowująca jednomian.
 * Jednomian ma postać @f$px_i^n@f$.
 * Współczynnik @f$p@f$ może też być
 * wielomianem nad kolejną zmienną @f$x_{i+1}@f$.
 */
typedef struct Mono {
  Poly p; ///< współczynnik
  poly_exp_t exp; ///< wykładnik
} Mono;


static inline size_t max(size_t x, size_t y){
    if(x > y) return x;
    else return y;
}

/**
 * Daje wartość wykładnika jendomianu.
 * @param[in] m : jednomian
 * @return wartość wykładnika jednomianu
 */
static inline poly_exp_t MonoGetExp(const Mono *m) {
  return m->exp;
}

/**
 * Tworzy wielomian, który jest współczynnikiem (wielomian stały).
 * @param[in] c : wartość współczynnika
 * @return wielomian
 */
static inline Poly PolyFromCoeff(poly_coeff_t c) {
  return (Poly) {.coeff = c, .arr = NULL};
}

/**
 * Tworzy wielomian tożsamościowo równy zeru.
 * @return wielomian
 */
static inline Poly PolyZero(void) {
  return PolyFromCoeff(0);
}

/**
 * Tworzy jednomian tożsamościowo równy zeru.
 * @return jednomian
 */
static inline Mono MonoZero(void) {
  return (Mono) {.p = PolyZero(), .exp = 1};
}

static inline bool PolyIsZero(const Poly *p);

/**
 * Tworzy jednomian @f$px_i^n@f$.
 * Przejmuje na własność zawartość struktury wskazywanej przez @p p.
 * @param[in] p : wielomian - współczynnik jednomianu
 * @param[in] n : wykładnik
 * @return jednomian @f$px_i^n@f$
 */
static inline Mono MonoFromPoly(const Poly *p, poly_exp_t n) {
  assert(n == 0 || !PolyIsZero(p));
  return (Mono) {.p = *p, .exp = n};
}

/**
 * Sprawdza, czy wielomian jest współczynnikiem (czy jest to wielomian stały).
 * @param[in] p : wielomian
 * @return Czy wielomian jest współczynnikiem?
 */
static inline bool PolyIsCoeff(const Poly *p) {
  return p->arr == NULL;
}

static inline bool PolyIsSingularlyDeepCoeff(const Poly *p) {
    return p->arr != NULL && p->size == 1 && p->arr[0].exp == 0 && p->arr[0].p.arr == NULL;
}

/**
 * Sprawdza, czy wielomian jest tożsamościowo równy zeru.
 * @param[in] p : wielomian
 * @return Czy wielomian jest równy zeru?
 */
static inline bool PolyIsZero(const Poly *p) {
  return PolyIsCoeff(p) && p->coeff == 0;
}

void PolyArrZero(Poly *p, size_t start, size_t end);

void PolyReall(Poly *p, size_t newSize);

void PolyReallFromTo(Poly* p, int fromIndex, int toIndex);

Poly* PolyAlloc(size_t size);

void PolyArrAlloc(Poly *new, size_t size);

void PolyRmEndData(Poly *p);

void PolyDelZeroMono(Poly *p);

void PolyLayerDelZeroMono(Poly *p);

void PolyLayerSimplify(Poly *p);

static inline void PolySimplify(Poly *p){
    if(!PolyIsCoeff(p)){
        for(int i = 0; i < p->size; ++i){
            PolySimplify(&(p->arr[i].p));
        }
        PolyLayerSimplify(p);
    }
}

static inline void MonoSort(Mono* m);

static inline int MonoExpCmp(const void * a, const void * b){
    Mono *_a = (Mono*)a;
    Mono *_b = (Mono*)b;
    if(_a->exp < _b->exp) return -1;
    else if(_a->exp == _b->exp) return 0;
    else return 1;
}

void PolySortHelp(Poly *p);

void PolySort(Poly *p);

bool PolyIsSorted(Poly p);

void PolyLayerSortHelp(Poly *p);

void PolyLayerSort(Poly *p);

static inline void MonoSort(Mono* m){
    PolySortHelp(&(m->p));
}

static inline void MonoDestroy(Mono *m);

/**
 * Usuwa wielomian z pamięci.
 * @param[in] p : wielomian
 */
static inline void PolyDestroy(Poly *p){
    if(!PolyIsCoeff(p)){
        for(poly_exp_t i = 0; i < p->size; ++i){
            MonoDestroy(&(p->arr[i]));
        }
        free(p->arr);
        p->arr = NULL;
    }
}

/**
 * Usuwa jednomian z pamięci.
 * @param[in] m : jednomian
 */
static inline void MonoDestroy(Mono *m) {
    PolyDestroy(&m->p);
}

/**
 * Robi pełną, głęboką kopię wielomianu.
 * @param[in] p : wielomian
 * @return skopiowany wielomian
 */
Poly PolyClone(const Poly *p);

/**
 * Robi pełną, głęboką kopię jednomianu.
 * @param[in] m : jednomian
 * @return skopiowany jednomian
 */
static inline Mono MonoClone(const Mono *m) {
    return (Mono) {.p = PolyClone(&m->p), .exp = m->exp};
}

void MonoPrintHelp(Mono m, int index);

void MonoPrint(Mono m, int index);

void PolyPrintHelp(Poly poly, int index);

static inline void PolyPrint(Poly poly, int index){
    PolyPrintHelp(poly, index);
    printf("\n");
}

void PolyComplete(Poly *to, size_t i1, Poly *from, size_t i2);

#include "polyAdd.h"
#include "polyMul.h"
#include "polyNegSub.h"
#include "polyDegEqAt.h"

#endif /* __POLY_H__ */
