#include "poly.h"
#include "polyDegEqAt.h"
#include "polyAdd.h"

#define CHECK_PTR(p)  \
  do {                \
    if (p == NULL) {  \
      exit(1);        \
    }                 \
  } while (0)

void PolyArrZero(Poly *p, size_t start, size_t end){
    for(size_t i = start; i < end; ++i){
        p->arr[i].exp = 1;
        p->arr[i].p = PolyZero();
    }
}

void PolyReall(Poly *p, size_t newSize){
    assert(newSize > 0);
    size_t old = p->size;
    if(newSize > p->size){
        p->size = newSize;
        p->arr = realloc(p->arr, p->size * sizeof(Mono));
        CHECK_PTR(p->arr);
        PolyArrZero(p, old, p->size);
    }
    else if(newSize < p->size){
        p->size = newSize;
        for(int i = newSize; i < old; ++i) MonoDestroy(&(p->arr[i]));
        p->arr = realloc(p->arr, p->size * sizeof(Mono));
        CHECK_PTR(p->arr);
    }
}

void PolyReallFromTo(Poly* p, int fromIndex, int toIndex){
    printf("PolyReallFromTo: p:%p f:%d t:%d\n",p ,fromIndex, toIndex);
    assert(toIndex - fromIndex + 1 > 0);
    Mono *newArr = malloc((toIndex - fromIndex + 1) * sizeof (Mono));
    for(int i = 0; i < (toIndex - fromIndex + 1); ++i){
        newArr[i] = p->arr[i + fromIndex];
    }
    p->size = toIndex - fromIndex + 1;
    free(p->arr);
    p->arr = newArr;
}

Poly* PolyAlloc(size_t size){
    assert(size > 0);
    Poly *new = malloc(sizeof(Poly));
    CHECK_PTR(new);
    new->size = size;
    new->arr = malloc(new->size * sizeof(Mono));
    CHECK_PTR(new->arr);
    PolyArrZero(new, 0, new->size);
    return new;
}

void PolyArrAlloc(Poly *new, size_t size){
    assert(size > 0);
    //if(new->arr != NULL) free(new->arr);
    new->size = size;
    new->arr = malloc(new->size * sizeof(Mono));
    CHECK_PTR(new->arr);
    PolyArrZero(new, 0, new->size);
}

void PolyRmEndData(Poly *p){
    bool endOfData = false;
    int eOD = 0;
    for(int i = 0; i < p->size; ++i){
        if(!endOfData && PolyIsZero(&(p->arr[i].p)) && p->arr[i].exp == 1){
            eOD = i;
            endOfData = true;
        }
        else if(!PolyIsZero(&(p->arr[i].p)) || p->arr[i].exp != 1){
            endOfData = false;
        }
    }
    if(endOfData && eOD > 0) PolyReall(p, eOD);
}


void PolyDelZeroMono(Poly *p){ //0 x_0^0 size 1
    if(PolyIsCoeff(p)) return;
    int slow = 0, fast = 1;
    while(slow < p->size && PolyDegSorted(&(p->arr[slow].p)) == -1){
        MonoDestroy(&(p->arr[slow]));
        slow++;
        if(slow >= p->size){
            free(p->arr);
            *p = PolyZero();
            return;
        }
    }
    p->arr[0] = p->arr[slow];
    for(fast = slow + 1, slow = 0; slow < p->size && fast < p->size;){
        while(fast < p->size && PolyDegSorted(&(p->arr[fast].p)) == -1){
            MonoDestroy(&(p->arr[fast]));
            fast++;
        }
        if(fast < p->size){
            p->arr[++slow] = p->arr[fast++];
        }
    }
    PolyReall(p, slow + 1);
}

void PolyLayerDelZeroMono(Poly *p){
    if(PolyIsCoeff(p)) return;
    int slow = 0, fast = 1;

    while(slow < p->size && PolyIsCoeff(&(p->arr[slow].p)) && p->arr[slow].p.coeff == 0){
        MonoDestroy(&(p->arr[slow]));
        slow++;
        if(slow >= p->size){
            free(p->arr);
            *p = PolyZero();
            return;
        }
    }
    p->arr[0] = p->arr[slow];
    for(fast = slow + 1, slow = 0; slow < p->size && fast < p->size;){
        while(fast < p->size && PolyIsCoeff(&(p->arr[fast].p)) && p->arr[fast].p.coeff == 0){
            MonoDestroy(&(p->arr[fast]));
            fast++;
        }
        if(fast < p->size){
            p->arr[++slow] = p->arr[fast++];
        }
    }
    PolyReall(p, slow + 1);
}

void PolyDelSingularlyDeepCoeff(Poly *p){
    if(PolyIsSingularlyDeepCoeff(p)){
        Poly new = PolyFromCoeff(p->arr[0].p.coeff);
        free(p->arr);
        *p = new;
    }
}

/**
 * Upraszcza wielomian pod wskaźnikiem p
 * Pamięć zwalniana przez funkcje: PolyAddSorted oraz PolyReall
 * @param[in] p : wskaźnik na *posortowany* wielomian
 */
void PolyLayerSimplify(Poly *p){
    poly_exp_t slow, fast;

    for(slow = 0, fast = 1; slow < p->size && fast < p->size;){
        while(fast < p->size && p->arr[slow].exp == p->arr[fast].exp){
            p->arr[slow].p = PolyAddSorted(&(p->arr[slow].p), &(p->arr[fast].p));
            p->arr[fast] = MonoZero(); //ochrona pamięci wielomianu pod indeksem fast
            fast++;
        }
        if(fast < p->size) {
            p->arr[++slow] = p->arr[fast++];
            if(slow != fast - 1)
              p->arr[fast - 1] = MonoZero();
        }
        assert(slow < fast);
    }
    if(slow + 1 < p->size) PolyReall(p, slow + 1);
}

void PolySortHelp(Poly *p){
    if(!PolyIsCoeff(p)){
        for(poly_exp_t i = 0; i < p->size; ++i){
            MonoSort(&(p->arr[i]));
        }
        qsort(p->arr, p->size, sizeof(Mono), MonoExpCmp);
        PolyLayerSimplify(p);
        PolyLayerDelZeroMono(p);
        PolyDelSingularlyDeepCoeff(p);
    }
}

void PolySort(Poly *p) {
    if (!PolyIsCoeff(p)) {
        PolySortHelp(p);
    }
}

bool PolyIsSorted(Poly p){
    bool res = true;
    poly_exp_t prev_exp = 0;
    if (!PolyIsCoeff(&p)) {
        return true;
    }
    else{
        for(poly_exp_t i = 0; i < p.size; ++i){
            res &= PolyIsSorted(p.arr[i].p);
            for(int i = 0; i < p.size; i++){
                res &= (p.arr[i].exp >= prev_exp);
                prev_exp = p.arr[i].exp;
            }
        }
    }
    return res;
}

void PolyLayerSortHelp(Poly *p){
    if(!PolyIsCoeff(p)){
        qsort(p->arr, p->size, sizeof(Mono), MonoExpCmp);
        PolyLayerSimplify(p);
        PolyLayerDelZeroMono(p);
        PolyDelSingularlyDeepCoeff(p);
    }
}

void PolyLayerSort(Poly *p) {
    if (!PolyIsCoeff(p)) {
        PolyLayerSortHelp(p);
    }
}

Poly PolyClone(const Poly *p){
    if(!PolyIsCoeff(p)){
        Poly new;
        PolyArrAlloc(&new, p->size);
        for(poly_exp_t i = 0; i < p->size; ++i){
            new.arr[i] = MonoClone(&(p->arr[i]));
        }
        return new;
    }
    else{
        return (Poly) {.coeff = p->coeff, .arr = NULL};
    }
}

void MonoPrintHelp(Mono m, int index){
    printf("(");
    PolyPrintHelp(m.p, index + 1);
    printf(")x_%d^%d", index, m.exp);
}

void MonoPrint(Mono m, int index){
    MonoPrintHelp(m, index);
    puts("");
}

void PolyPrintHelp(Poly poly, int index){
    if(PolyIsCoeff(&poly)){
        printf("%ld", poly.coeff);
    }
    else{
        for(int i = 0; i < poly.size; ++i){
            MonoPrintHelp(poly.arr[i], index);
            if(i < poly.size - 1) printf(" + ");
        }
    }
}

void PolyComplete(Poly *to, size_t i1, Poly *from, size_t i2){
    while(i2 < from->size){
        assert(i1 <= to->size);
        if(i1 == to->size){
            PolyReall(to, to->size * 2);
        }
        to->arr[i1++] = from->arr[i2++];
    }
    PolyRmEndData(to);
}


