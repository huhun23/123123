#ifndef POLYNOMIALS_POLYADD_H
#define POLYNOMIALS_POLYADD_H

Mono MonoAddSorted(Mono *m1, Mono *m2);

Mono MonoAdd(Mono *m1, Mono *m2);

Poly PolyAddNonCoeffs(Poly *p1, Poly *q1);

Poly PolyAddSorted(Poly *p1, Poly *q1);

void PolyAppend(Poly *p, Poly *q);

/**
 * Dodaje dwa wielomiany.
 * @param[in] p : wielomian @f$p@f$
 * @param[in] q : wielomian @f$q@f$
 * @return @f$p + q@f$
 */
Poly PolyAdd(const Poly *p, const Poly *q);

/**
 * Sumuje listę jednomianów i tworzy z nich wielomian.
 * Przejmuje na własność zawartość tablicy @p monos.
 * @param[in] count : liczba jednomianów
 * @param[in] monos : tablica jednomianów
 * @return wielomian będący sumą jednomianów
 */
Poly PolyAddMonos(size_t count, const Mono monos[]);

Poly PolyAddMonosDbg(size_t count, const Mono monos[]);

#endif //POLYNOMIALS_POLYADD_H