#include "poly.h"
#include "polyMul.h"

/* Funkcja, która pomaga mnożyć dwa wielomiany
 * Pamięć pod p1 i q1 jest wykorzystywana do utworzenia nowego wielomianu?
 *
 */
Poly PolyMulHelp(Poly *p1, Poly *q1){
    if(!PolyIsCoeff(p1) && !PolyIsCoeff(q1)){
        Poly new;
        PolyArrAlloc(&new, p1->size * q1->size);
        int k = 0;
        for(size_t i = 0; i < p1->size; ++i){
            for(size_t j = 0; j < q1->size; ++j){
                Poly p2 = PolyClone(&(p1->arr[i].p));
                Poly q2 = PolyClone(&(q1->arr[j].p));
                new.arr[k].p = PolyMulHelp(&p2, &q2);
                new.arr[k].exp = p1->arr[i].exp + q1->arr[j].exp;
                k++;
            }
        }
        PolyDestroy(p1);
        PolyDestroy(q1);
        PolySort(&new);
        return new;
    }
    else if(!PolyIsCoeff(q1)){
        return PolyMulHelp(q1, p1);
    }
    else if(!PolyIsCoeff(p1)){ //p1 nie jest współczynnikiem
        for(poly_exp_t i = 0; i < p1->size; ++i){
            p1->arr[i].p = PolyMulHelp(&(p1->arr[i].p), q1);

        }
        return *p1;
    }
    else{
        return (Poly) {.coeff = p1->coeff * q1->coeff, .arr = NULL};
    }
}
