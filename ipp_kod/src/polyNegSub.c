#include "poly.h"
#include "polyNegSub.h"

void PolyNegPtr(Poly *p){
    if(PolyIsCoeff(p)){
        p->coeff = -1 * p->coeff;
        p->arr = NULL;
    }
    else{
        for(poly_exp_t i = 0; i < p->size; ++i){
            MonoNeg(&(p->arr[i]));
        }
    }
}

Poly PolyNeg(const Poly *p){
  Poly new = PolyClone(p);
  PolyNegPtr(&new);
  return new;
}
