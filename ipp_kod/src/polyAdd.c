#include "poly.h"
#include "polyAdd.h"
#include "polyDegEqAt.h"


Mono MonoAddSorted(Mono *m1, Mono *m2){
  assert(m1->exp == m2->exp);
  Poly p = PolyAddSorted(&(m1->p), &(m2->p));

  if(m1->exp == 0 && PolyDeg(&p) == -1){
    return (Mono) {.exp = 1, .p = PolyZero()};
  }
  else {
    return (Mono) {.exp = m1->exp, .p = p};
  }
}

Mono MonoAdd(Mono *m1, Mono *m2){
    assert(m1->exp == m2->exp);
    MonoSort(m1);
    MonoSort(m2);
    return MonoAddSorted(m1, m2);
}

//z pamięci zajmowanej przez p1 i q1 tworzy nowy wielomian
//zajmuje się pamięcią p1 i q1
Poly PolyAddNonCoeffs(Poly *p1, Poly *q1){
    //p1 i q1 posortowane
    Poly new;
    PolyArrAlloc(&new, max(p1->size, q1->size));
    poly_exp_t i = 0, j = 0, k = 0;

    for(k = 0; i < p1->size && j < q1->size; ++k){
        assert(k <= new.size);
        if(k == new.size){
            PolyReall(&new, new.size * 2);
        }
        if(p1->arr[i].exp < q1->arr[j].exp){
            new.arr[k] = p1->arr[i];
            i++;
        }
        else if(p1->arr[i].exp > q1->arr[j].exp){
            new.arr[k] = q1->arr[j];
            j++;
        }
        else{
            Mono *m1 = &(p1->arr[i]), *m2 = &(q1->arr[j]);
            new.arr[k] = MonoAddSorted(m1, m2);
            i++;
            j++;
        }
    }

    if(i >= p1->size){
        PolyComplete(&new, k, q1, j);
    }
    else if(j >= q1->size) {
        PolyComplete(&new, k, p1, i);
    }

    //printf("us: \n");
    //PolyPrint(new, 0);
    PolyLayerSort(&new);
    //printf("s: \n");
    //PolyPrint(new, 0);

    free(p1->arr);
    free(q1->arr);

    return new;
}


//z pamięci zajmowanej przez p1 i q1 tworzy nowy wielomian
//zajmuje się pamięcią p1 i q1
Poly PolyAddSorted(Poly *p1, Poly *q1){
    Poly new;
    if(!PolyIsCoeff(p1) && !PolyIsCoeff(q1)){
        return PolyAddNonCoeffs(p1, q1);
    }
    else if(!PolyIsCoeff(q1)){
        return PolyAddSorted(q1, p1);
    }
    else if(!PolyIsCoeff(p1)){ //p1 nie jest współczynnikiem
        assert(p1->arr != NULL && p1->size > 0);
        if(p1->arr[0].exp == 0){
            //Poly *foo = &(p1->arr[0].p);
            p1->arr[0].p = PolyAddSorted(&(p1->arr[0].p), q1);
        }
        else{
            PolyReall(p1, p1->size + 1);
            for(int i = p1->size - 1; i >= 1; --i){
                p1->arr[i].p = p1->arr[i - 1].p;
                p1->arr[i].exp = p1->arr[i - 1].exp;
            }
            p1->arr[0].p = *q1;
            p1->arr[0].exp = 0;
        }
        return *p1;
    }
    else{
        new.coeff = p1->coeff + q1->coeff;
        new.arr = NULL;
        return new;
    }
}

//klonuje p i q, nie zajmuje się pamięcią przez nie zajmowaną
Poly PolyAdd(const Poly *p, const Poly *q){
    Poly new;
    Poly p1 = PolyClone(p), q1 = PolyClone(q);
    PolySort(&p1);
    PolySort(&q1);
    /*printf("in: \n");
    PolyPrint(*p, 0);
    PolyPrint(*q, 0);*/

    new = PolyAddSorted(&p1, &q1);

    //printf("out: \n");
    //PolyPrint(new, 0);
    return new;
}

Poly PolyAddMonos(size_t count, const Mono monos[]){
    Poly new;
    PolyArrAlloc(&new, count);

    for(poly_exp_t i = 0; i < count; ++i){
        new.arr[i] = monos[i];
    }

    PolyLayerSort(&new);
    return new;
}

